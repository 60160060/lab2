module.exports = {
    isUserNameValid: function (username) {
        if(username.length < 3 || username.length > 15) {
            return false;
        }
        // Kob -> kob
        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;    
    }
    ,
    isAgeValid: function (age) {
        if (isNaN(parseInt(age))) {
            return false;
        }
        if (parseInt(age) < 18 || parseInt(age) > 100) {
            return false;
        }
        return true;
    }
    ,
    isPasswordValid: function (password) {
        if (password.length < 8) {
            return false;
        }
        var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
        var character = '';
        var status = false;
        var stackinteger = 0;
        var stackspecial = 0;
        for (var i = 0; i < password.length; i++) {
            character = password.charAt(i);
            if (i == password.length-1 && status == false) {
                return false;
            }
            if (!isNaN(parseInt(character))){
                stackinteger++;
                continue;
            }
            if (format.test(character)) {
                stackspecial++;
                continue;
            } 
            if (character.toLowerCase() == character) {
                continue;
            }
            if (character.toUpperCase() == character) {
                status = true;
            }          
        }
        if (stackinteger < 3) {
            return false;
        }
        if (stackspecial < 1) {
            return false
        }
       return true;
    }
    ,
    isDateValid: function (day,month,year) {
        if (parseInt(day) < 1 || parseInt(day) > 31 ) {
            return false;
        }
        if (parseInt(month) < 1 || parseInt(month) > 12 ) {
            return false;
        }
        if (parseInt(year) < 1970 || parseInt(year) > 2020 ) {
            return false;
        }
        switch (parseInt(month)) {
            case 1:
                    if (parseInt(day) < 1 || parseInt(day) > 31) {
                        return false;   
                    }
                    break;
            case 2:
                    if (parseInt(year) % 4 == 0) {
                        if (parseInt(day) < 1 || parseInt(day) > 29) {
                            return false;   
                        }   
                    }else{
                        if (parseInt(day) < 1 || parseInt(day) > 28) {
                            return false;   
                        }   
                    }
                    break;
            case 3:
                    if (parseInt(day) < 1 || parseInt(day) > 31) {
                        return false;   
                    }
                    break;
            case 4:
                if (parseInt(day) < 1 || parseInt(day) > 30) {
                    return false;   
                }
                break;
            case 5:
                    if (parseInt(day) < 1 || parseInt(day) > 31) {
                        return false;   
                    }
                    break;
            case 6:
                    if (parseInt(day) < 1 || parseInt(day) > 30) {
                        return false;   
                    }
                    break;
            case 7:
                    if (parseInt(day) < 1 || parseInt(day) > 31) {
                        return false;   
                    }
                    break;
            case 8:
                if (parseInt(day) < 1 || parseInt(day) > 31) {
                    return false;   
                }
                break;
            case 9:
                    if (parseInt(day) < 1 || parseInt(day) > 30) {
                        return false;   
                    }
                    break;
            case 10:
                    if (parseInt(day) < 1 || parseInt(day) > 31) {
                        return false;   
                    }
                    break;
            case 11:
                    if (parseInt(day) < 1 || parseInt(day) > 30) {
                        return false;   
                    }
                    break;
            case 12:
                if (parseInt(day) < 1 || parseInt(day) > 31) {
                    return false;   
                }
                break;
        }
        return true;
    }
}